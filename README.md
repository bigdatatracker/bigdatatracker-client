# Bigdata tracker (Frontend)

## Code style

[Airbnb](https://github.com/airbnb/javascript)

## Code conventions

1. Unless specified otherwise in Airbnb code style guide - [please follow these javascript clean code principles](https://github.com/ryanmcdermott/clean-code-javascript)
2. [Use named exports instead of default](https://humanwhocodes.com/blog/2019/01/stop-using-default-exports-javascript-module/)
3. [named files - no index.ts(x)](https://bradfrost.com/blog/post/this-or-that-component-names-index-js-or-component-js/)

## Workflow convention

1. Trunk Based Development - [example](https://trunkbaseddevelopment.com)
2. Feature -> Master

## Commit strategy

1. git commit -m "some commit" -> after that runs typecheck, prettier and eslint. If typecheck or eslint fails there will be no commit.
2. To skip errors after commit just run command git commit -m "some commit" --no-verify (Don't do it unnecessarily)
