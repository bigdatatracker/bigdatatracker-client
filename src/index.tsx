import React from 'react';
import ReactDOM from 'react-dom';
import { ProvidedApp } from './modules/Core';

ReactDOM.render(<ProvidedApp />, document.getElementById('root'));
