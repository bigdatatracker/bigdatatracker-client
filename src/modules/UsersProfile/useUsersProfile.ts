import { useQuery } from 'react-query';

import { ajaxEvents } from '../../common/http';
import { PostType } from '../../common/types';

export const useUsersProfile = (id: string | undefined) => {
  const getUserById = async () => {
    const { data } = await ajaxEvents.request<any>({
      method: 'GET',
      url: `/users/${id}`,
    });
    return data;
  };

  const getPostsById = async () => {
    const {
      data: { items },
    } = await ajaxEvents.request<{ items: PostType[] }>({
      method: 'GET',
      url: `/users/${id}/posts`,
      params: {
        limit: 50,
      },
    });

    return items.reverse().slice(0, 5);
  };

  const {
    data,
    isError: error,
    isLoading: loading,
  } = useQuery(['usersProfile', id], () => getUserById(), {
    refetchOnWindowFocus: false,
  });

  const {
    data: userPostsByid = [],
    isError: isErrorPostsByid,
    isLoading: isLoadingPostsByid,
    isRefetching: isRefetchingPostsByid,
    refetch: refetchPostsByid,
  } = useQuery(['userPostsByid'], () => getPostsById(), {
    refetchOnWindowFocus: false,
  });

  return {
    data,
    error,
    loading,
    postsByIdData: {
      userPostsByid,
      isErrorPostsByid,
      isLoadingPostsByid,
      isRefetchingPostsByid,
      refetchPostsByid,
    },
  };
};
