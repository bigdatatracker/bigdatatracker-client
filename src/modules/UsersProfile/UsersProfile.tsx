import React from 'react';
import { CircularProgress, styled } from '@mui/material';

import { useParams } from 'react-router-dom';
import { Post } from '../Home/components';
import { INDENT } from '../../common/tokens';

import { UserInfo } from './components/UserInfo/UserInfo';
import { useUsersProfile } from './useUsersProfile';
import { PageLayout } from '../PageLayout';
import { withAuth0UserProvider } from '../../common/HOC';

const ContentWrapper = styled('div')({
  display: 'flex',
});

const UserInfoWrapper = styled('div')({
  margin: `${INDENT.xxl} ${INDENT.xxl}`,
});

const PostsWrapper = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  maxWidth: 800,
  margin: `${INDENT.none} ${INDENT.xxl}`,
  width: '100%',
});

const UsersProfileComponent = () => {
  const { id } = useParams<string>();
  const {
    data,
    loading,
    postsByIdData: {
      userPostsByid,
      refetchPostsByid,
      isLoadingPostsByid,
      isRefetchingPostsByid,
    },
  } = useUsersProfile(id);

  React.useEffect(() => {
    if (id) {
      refetchPostsByid();
    }
  }, [id, refetchPostsByid]);

  if (loading) {
    return <CircularProgress />;
  }

  return (
    <PageLayout>
      <ContentWrapper>
        <UserInfoWrapper>
          <UserInfo {...data} />
        </UserInfoWrapper>
        <PostsWrapper>
          {isLoadingPostsByid || isRefetchingPostsByid ? (
            <CircularProgress />
          ) : (
            userPostsByid.map((post, index) => (
              <Post
                // eslint-disable-next-line react/no-array-index-key
                key={`${post.content}_${index}`}
                title={post.content}
                postIndex={index + 1}
              />
            ))
          )}
        </PostsWrapper>
      </ContentWrapper>
    </PageLayout>
  );
};

export const UsersProfilePage = withAuth0UserProvider(UsersProfileComponent);
