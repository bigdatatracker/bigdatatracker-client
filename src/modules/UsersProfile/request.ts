import { ajaxEvents } from '../../common/http';

export const getUserById = async (id: string | undefined) => {
  const { data } = await ajaxEvents.request<any>({
    method: 'GET',
    url: `/users/${id}`,
  });

  return data;
};
