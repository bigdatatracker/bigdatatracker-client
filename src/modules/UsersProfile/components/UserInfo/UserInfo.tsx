import React from 'react';
import { useMutation } from 'react-query';

import { styled, Typography, Avatar } from '@mui/material';
import LoadingButton from '@mui/lab/LoadingButton';
import StarIcon from '@mui/icons-material/Star';

import { UserInfoProps } from './types';
import { ajaxEvents } from '../../../../common/http';
import { AuthUserContext } from '../../../../common/context';

const AvatarWrapper = styled('div')({
  display: 'flex',
  alignItems: 'center',
});

const NameWrapper = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  alignContent: 'center',
  alignItems: 'center',
  marginLeft: 30,
});

const BalanceWrapper = styled('div')({
  display: 'flex',
  alignItems: 'center',
});

const DescriptionWrapper = styled('div')({
  marginTop: 10,
});

const ManagerAvatarWrapper = styled('div')({
  display: 'flex',
  alignContent: 'center',
  alignItems: 'center',
});

const ManagerWrapper = styled('div')({
  marginTop: 10,
});

const employUser = async ({ id }: { id: string }) => {
  return ajaxEvents.request<any>({
    method: 'POST',
    url: `/users/${id}/employ`,
    data: {
      id,
    },
  });
};

export const UserInfo = ({
  name,
  picture,
  status,
  bio,
  role,
  balance,
  position,
  manager,
  id,
}: UserInfoProps) => {
  const { userInfo, logout } = React.useContext(AuthUserContext);
  const {
    mutate,
    isLoading: isEmployUserLoading,
    isError: isErrorEmployUser,
  } = useMutation(employUser);

  return (
    <>
      <AvatarWrapper>
        <Avatar src={picture} alt={name} sx={{ width: 120, height: 120 }} />
        <NameWrapper>
          <Typography variant="h6">{name} </Typography>
          <BalanceWrapper>
            <StarIcon color="success" sx={{ marginRight: 1 }} />
            <Typography fontSize={16}>{balance}</Typography>
          </BalanceWrapper>
          <Typography fontSize={14}>{position}</Typography>
        </NameWrapper>
      </AvatarWrapper>
      <DescriptionWrapper>
        <Typography fontSize={14} fontWeight={600} color="primary">
          О себе
        </Typography>
        <Typography fontSize={16}>{bio}</Typography>
      </DescriptionWrapper>

      {userInfo?.isManager && manager ? (
        <ManagerWrapper>
          <Typography fontSize={14} fontWeight={600} color="primary">
            Менеджер
          </Typography>
          <ManagerAvatarWrapper>
            <Avatar
              src={manager.picture}
              alt={manager.name}
              sx={{ width: 30, height: 30, marginRight: 1 }}
            />
            <Typography>{manager.name}</Typography>
          </ManagerAvatarWrapper>
        </ManagerWrapper>
      ) : (
        userInfo?.isManager &&
        userInfo?.userId !== id && (
          <>
            <LoadingButton
              loading={isEmployUserLoading}
              variant="contained"
              onClick={() => mutate({ id })}
              sx={{ marginTop: 2 }}
            >
              Взять в подчинённые
            </LoadingButton>

            {isErrorEmployUser && (
              <Typography> Произошла ошибка. Поробуйте еще раз</Typography>
            )}
          </>
        )
      )}
    </>
  );
};
