// Временно оставляем, т.к. бэк пока не возвращает нужные поля
// В будущем использовать пока DB_USER_DATA
export type UserInfoProps = {
  name: string;
  role: string;
  bio: string;
  status: string;
  picture: string;
  balance: string;
  position: string;
  manager: ManagerProps;
  id: string;
};

type ManagerProps = {
  email: string;
  id: string;
  name: string;
  picture: string;
  role: string;
};
