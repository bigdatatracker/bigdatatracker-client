import React from 'react';

import { locale } from './locale';

const page404 = () => {
  return <div>{locale.page404Title}</div>;
};

export default page404;
