import React from 'react';
import { RouterConfig } from '../../common/router';

const Home = React.lazy(() => import('../Home'));
const Login = React.lazy(() => import('../Login'));
const Quests = React.lazy(() => import('../Quests'));
const ManageQuest = React.lazy(() => import('../ManageQuest'));
const UsersProfile = React.lazy(() => import('../UsersProfile'));
const Leaderboard = React.lazy(() => import('../Leaderboard'));
const EditProfile = React.lazy(() => import('../EditProfile'));
const QuestsAssignees = React.lazy(() => import('../QuestsAssignees'));
const page404 = React.lazy(() => import('./page404'));

export const ROUTES_PATHS_MAP = {
  PAGE404: '*',
  LOGIN: '/',
  HOME: '/home',
  QUESTS: '/quests',
  MANAGE_QUEST: '/quests/manage',
  USERS_PROFILE: '/profiles/:id',
  EDIT_PROFILE: '/edit',
  LEADERBOARD: '/leaderboard',
  QUEST_ASSIGNEES: '/quest-asignees/:id',
} as const;

// 404 need to be last
export const routes: RouterConfig = [
  {
    pageName: 'login_page',
    path: ROUTES_PATHS_MAP.LOGIN,
    component: Login,
  },
  {
    pageName: 'home_page',
    path: ROUTES_PATHS_MAP.HOME,
    component: Home,
  },
  {
    pageName: 'quests_page',
    path: ROUTES_PATHS_MAP.QUESTS,
    component: Quests,
  },
  {
    pageName: 'edit_quest_page',
    path: ROUTES_PATHS_MAP.MANAGE_QUEST,
    component: ManageQuest,
  },
  {
    pageName: 'users_profile_page',
    path: ROUTES_PATHS_MAP.USERS_PROFILE,
    component: UsersProfile,
  },
  {
    pageName: 'leaderboard_page',
    path: ROUTES_PATHS_MAP.LEADERBOARD,
    component: Leaderboard,
  },
  {
    pageName: 'edit_profile_page',
    path: ROUTES_PATHS_MAP.EDIT_PROFILE,
    component: EditProfile,
  },
  {
    pageName: 'quests-assignees',
    path: ROUTES_PATHS_MAP.QUEST_ASSIGNEES,
    component: QuestsAssignees,
  },
  {
    pageName: '404_page',
    path: ROUTES_PATHS_MAP.PAGE404,
    component: page404,
  },
];
