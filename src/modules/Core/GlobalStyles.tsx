import React from 'react';
import { GlobalStyles } from '@mui/material';

export const GlobalAppStyles = () => (
  <GlobalStyles
    styles={{
      html: {
        height: '100%',
        width: '100%',
      },
      '*, *::before, *::after': {
        boxSizing: 'inherit',
      },
      body: {
        height: '100%',
        width: '100%',
        margin: 0,
      },
      '#root': {
        height: '100%',
        width: '100%',
      },
    }}
  />
);
