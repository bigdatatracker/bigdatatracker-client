import React from 'react';
import { ThemeProvider } from '@mui/material';
import { Route } from 'react-router-dom';
import { Auth0Provider } from '@auth0/auth0-react';
import { QueryClient, QueryClientProvider } from 'react-query';
import { Provider } from 'react-redux';

import { RouterProvider, RouterConfig } from '../../common/router';
import {
  APP_DOMAIN,
  AUTH0_AUDIENCE,
  AUTH0_SCOPE,
  CLIENT_ID,
  THEME,
} from '../../common/constants';
import { store } from '../../store/store';

import { routes, ROUTES_PATHS_MAP } from './routeConfig';
import { locale } from './locale';
import { GlobalAppStyles } from './GlobalStyles';

const queryClient = new QueryClient();

export const ProvidedApp = () => {
  const renderRoutes = (routesConfig: RouterConfig) => {
    return routesConfig.map(({ path, component: Component, pageName }) => (
      <Route
        key={pageName}
        path={path}
        element={
          <React.Suspense fallback={locale.loadingFallback}>
            <Component />
          </React.Suspense>
        }
      />
    ));
  };

  return (
    <Provider store={store}>
      <ThemeProvider theme={THEME}>
        <QueryClientProvider client={queryClient}>
          <Auth0Provider
            domain={APP_DOMAIN}
            clientId={CLIENT_ID}
            audience={AUTH0_AUDIENCE}
            redirectUri={`${window.location.origin}${ROUTES_PATHS_MAP.HOME}`}
            scope={AUTH0_SCOPE}
          >
            <GlobalAppStyles />
            <RouterProvider>{renderRoutes(routes)}</RouterProvider>
          </Auth0Provider>
        </QueryClientProvider>
      </ThemeProvider>
    </Provider>
  );
};
