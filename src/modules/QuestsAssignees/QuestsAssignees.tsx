import { Error } from '@mui/icons-material';
import {
  Avatar,
  Checkbox,
  CircularProgress,
  Divider,
  FormControlLabel,
  FormGroup,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Typography,
} from '@mui/material';
import React from 'react';
import { useParams } from 'react-router-dom';
import { withAuth0UserProvider } from '../../common/HOC';
import { INDENT } from '../../common/tokens';
import { PageLayout } from '../PageLayout';
import { Quest } from '../Quests/components';
import { QuestData } from '../Quests/types';
import { useQuestsAssignees } from './useQuestsAssignees';

const QuestsAssigneesPage = () => {
  const { id } = useParams();
  const {
    questAssigneesById: users,
    loading,
    error,
    questById,
    questsListError,
    questsListLoading,
    approveQuest,
    isApproveQuestLoading,
    isRefetchingQuestAssignees,
  } = useQuestsAssignees(id);
  const [userQuestToApproveId, setUserQuestToApproveId] = React.useState<
    string | null
  >(null);

  if (loading || questsListLoading) {
    return <CircularProgress />;
  }

  if (error || questsListError) {
    return <Error />;
  }

  return (
    <PageLayout>
      <Quest {...(questById as QuestData)} removeActionLink />

      <Typography sx={{ marginTop: `${INDENT.l}`, textAlign: 'center' }}>
        {users?.length > 0
          ? 'Список пользователей взявших квест'
          : '  Данный квест пока никто не взял'}
      </Typography>
      <List
        sx={{ width: '100%', maxWidth: '100%', bgcolor: 'background.paper' }}
      >
        {isRefetchingQuestAssignees ? (
          <CircularProgress />
        ) : (
          users?.map(
            (user: {
              picture: string;
              email: string;
              name: string;
              id: string;
              questStatus: string;
            }) => (
              <React.Fragment key={user.email}>
                <ListItem alignItems="flex-start">
                  <ListItemAvatar>
                    <Avatar alt="Remy Sharp" src={user.picture} />
                  </ListItemAvatar>
                  <ListItemText primary={user.name} secondary={user.email} />
                  <FormGroup>
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={user.questStatus === 'done'}
                          disabled={user.questStatus === 'done'}
                          onChange={() => {
                            setUserQuestToApproveId(user.id);
                            approveQuest(user.id, questById.id);
                          }}
                        />
                      }
                      label="Апрувнуть квест"
                    />
                    {isApproveQuestLoading &&
                      userQuestToApproveId === user.id && (
                        <CircularProgress size={30} />
                      )}
                  </FormGroup>
                </ListItem>
                <Divider variant="inset" component="li" />
              </React.Fragment>
            ),
          )
        )}
      </List>
    </PageLayout>
  );
};

export const QuestsAssignees = withAuth0UserProvider(QuestsAssigneesPage);
