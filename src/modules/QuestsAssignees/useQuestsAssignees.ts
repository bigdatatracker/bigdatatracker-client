import { useMutation, useQuery } from 'react-query';

import { ajaxEvents } from '../../common/http';

export const useQuestsAssignees = (questId?: string) => {
  const getQuestsList = async () => {
    const { data } = await ajaxEvents.request<any>({
      method: 'GET',
      url: '/quests',
    });

    return data;
  };

  const getQuestAssignees = async () => {
    const { data } = await ajaxEvents.request<any>({
      method: 'GET',
      url: `/quests/${questId}/assignees`,
    });

    return data;
  };

  const approveQuestForUser = async ({
    userId,
    questToApproveId,
  }: {
    userId: string;
    questToApproveId: string;
  }) => {
    const { status } = await ajaxEvents.request<any>({
      method: 'POST',
      url: '/quests/approve',
      data: {
        userId,
        questId: questToApproveId,
      },
    });

    return status === 200;
  };

  const {
    data: questsList = [],
    isError: questsListError,
    isLoading: questsListLoading,
  } = useQuery('questsList', () => getQuestsList(), {
    refetchOnWindowFocus: false,
  });

  const {
    data: questAssigneesById = [],
    isError: error,
    isLoading: loading,
    refetch,
    isRefetching: isRefetchingQuestAssignees,
  } = useQuery('questAssigneesById', () => getQuestAssignees(), {
    enabled: !!questId,
    refetchOnWindowFocus: false,
  });

  const { mutateAsync: approveQuest, isLoading: isApproveQuestLoading } =
    useMutation(approveQuestForUser);

  const handleQuestApprove = async (
    userId: string,
    questToApproveId: string,
  ) => {
    await approveQuest({ userId, questToApproveId });
    await refetch();
  };

  const questById =
    questsList?.items?.length > 0
      ? questsList?.items?.find((item: any) => item.id === questId)
      : null;

  return {
    questAssigneesById,
    error,
    loading,
    questById,
    questsListError,
    questsListLoading,
    approveQuest: handleQuestApprove,
    isApproveQuestLoading,
    isRefetchingQuestAssignees,
  };
};
