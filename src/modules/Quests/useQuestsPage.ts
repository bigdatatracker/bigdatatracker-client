import { useQuery } from 'react-query';

import { ajaxEvents } from '../../common/http';
import { QuestData } from './types';

export const useQuestsPage = () => {
  const getQuestsList = async () => {
    const { data } = await ajaxEvents.request<any>({
      method: 'GET',
      url: '/quests',
    });

    return data;
  };

  const {
    data,
    isError: error,
    isLoading: loading,
  } = useQuery('questsList', () => getQuestsList());

  const questsList = data?.items?.sort((q1: QuestData, q2: QuestData) =>
    q1.status.localeCompare(q2.status),
  );

  const getQuestsHeader = () => {
    const hasQuests = questsList && questsList.length > 0;
    if (error) {
      return 'Произошла ошибка';
    }
    if (loading && !hasQuests) {
      return 'Идёт загрузка...';
    }
    return hasQuests
      ? 'Список доступных квестов'
      : 'Для вас пока нет доступных квестов';
  };

  const questsHeader = getQuestsHeader();

  return {
    data,
    error,
    loading,
    questsList,
    questsHeader,
  };
};
