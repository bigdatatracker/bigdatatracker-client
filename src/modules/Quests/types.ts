export type QuestData = {
  id: string;
  title: string;
  reward: string;
  status: string;
  creator: {
    id: string;
    name: string;
    role: string;
    picture: string;
  };
  description: string;
};
