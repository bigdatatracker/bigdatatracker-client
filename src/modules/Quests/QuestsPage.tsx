import React from 'react';
import { Box, Container, Typography } from '@mui/material';

import { Quest } from './components/Quest/Quest';
import { QuestData } from './types';
import { useQuestsPage } from './useQuestsPage';
import { PageLayout } from '../PageLayout';
import { withAuth0UserProvider } from '../../common/HOC';

const QuestsPageComponent = () => {
  const { questsList, questsHeader } = useQuestsPage();

  return (
    <PageLayout>
      <Box p={2}>
        <Container>
          <Typography variant="h4">{questsHeader}</Typography>
          {questsList &&
            questsList.map((quest: QuestData) => (
              <Quest {...quest} key={quest.id} />
            ))}
        </Container>
      </Box>
    </PageLayout>
  );
};

export const QuestsPage = withAuth0UserProvider(QuestsPageComponent);
