import React from 'react';
import { ExpandMore } from '@mui/icons-material';
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Avatar,
  styled,
  Typography,
} from '@mui/material';

import { LoadingButton } from '@mui/lab';
import { useMutation } from 'react-query';
import { QuestData } from '../../types';
import { ajaxEvents } from '../../../../common/http';

const DetailLine = styled('div')(() => ({
  display: 'flex',
  alignItems: 'center',
  flexWrap: 'wrap',
}));

const User = styled('div')(() => ({
  display: 'inline-flex',
  justifyItems: 'center',
  alignItems: 'center',
  flexWrap: 'wrap',
  marginLeft: 5,
}));

const statusMap = {
  available: 'доступен',
  taken: 'занят',
  creator: 'создан',
  done: 'выполнен',
};

export const Quest = (
  questData: QuestData & { removeActionLink?: boolean },
) => {
  const {
    id: questId,
    title,
    reward,
    status: initialStatus,
    description,
    creator,
    removeActionLink,
  } = questData;

  const [status, setStatus] = React.useState(initialStatus);

  const handleAssignQuest = async () => {
    const rsp = await ajaxEvents.request<any>({
      method: 'POST',
      url: `/quests/${questId}/assign`,
    });

    if (rsp.status < 400) {
      setStatus('taken');
    }
  };

  const { mutate: assignQuest, isLoading } = useMutation(handleAssignQuest);

  const { name: creatorName, picture: creatorAvatarUrl } = creator;

  const humanReadableStatus =
    statusMap[status as keyof typeof statusMap] || 'ошибка';

  return (
    <Accordion defaultExpanded key={questId}>
      <AccordionSummary expandIcon={<ExpandMore />}>
        <div>
          <Typography variant="h6">{title}</Typography>
          <Typography variant="body1">{description}</Typography>
        </div>
      </AccordionSummary>
      <AccordionDetails>
        <DetailLine>
          <Typography alignItems="center" display="inline-flex">
            Создатель:
          </Typography>
          <User>
            <Avatar alt={creatorName} src={creatorAvatarUrl} />
            <Typography paragraph={false} marginLeft={1}>
              {creatorName}
            </Typography>
          </User>
        </DetailLine>
        <Typography>Статус: {humanReadableStatus}</Typography>
        <Typography>Награда: {reward}</Typography>

        {!removeActionLink && (
          <LoadingButton
            loading={isLoading}
            disabled={status !== 'available'}
            onClick={() => assignQuest()}
            variant="contained"
            sx={{ mt: 1 }}
          >
            Начать
          </LoadingButton>
        )}
      </AccordionDetails>
    </Accordion>
  );
};
