import { DB_USER } from '../../common/types';

export type NewQuestData = {
  title: string;
  reward: number;
  description: string;
};

export type apiQuestData = {
  title: string;
  reward: number;
  description: string;
  id: string;
  status: string;
  creator: DB_USER;
};
