import React from 'react';
import { useMutation, useQuery } from 'react-query';

import { ajaxEvents } from '../../common/http';
import { AuthUserContext } from '../../common/context';

import { NewQuestData } from './types';

export const useManageQuest = () => {
  const { userInfo } = React.useContext(AuthUserContext);

  const [title, setTitle] = React.useState('');
  const [reward, setReward] = React.useState('');
  const [description, setDescription] = React.useState('');

  const handleCreateQuest = async (questData: NewQuestData) => {
    await ajaxEvents.request<any>({
      method: 'POST',
      url: `/quests`,
      data: questData,
    });
  };

  const getMyQuests = async () => {
    const { data } = await ajaxEvents.request<any>({
      method: 'GET',
      url: `/quests`,
      params: { creator: userInfo?.userId },
    });

    return data;
  };

  const {
    mutate: createQuest,
    isLoading: createIsLoading,
    isError: createIsError,
    isSuccess: createIsSuccess,
  } = useMutation(handleCreateQuest);

  const {
    data: myQuestsData,
    isError: myQuestsError,
    isLoading: myQuestsLoading,
  } = useQuery('myQuestsList', getMyQuests, { enabled: !!userInfo });

  const myQuests = myQuestsData?.items;

  return {
    title,
    setTitle,
    reward,
    setReward,
    description,
    setDescription,

    createQuest,
    createIsLoading,
    createIsError,
    createIsSuccess,

    myQuests,
    myQuestsError,
    myQuestsLoading,
  };
};
