import React from 'react';
import {
  Alert,
  Box,
  Container,
  TextField,
  Typography,
  ListItemText,
  ListItemButton,
  CircularProgress,
} from '@mui/material';
import { LoadingButton } from '@mui/lab';

import { PageLayout } from '../PageLayout';
import { withAuth0UserProvider } from '../../common/HOC';

import { useManageQuest } from './useManageQuest';
import { apiQuestData } from './types';

const ManageQuestComponent = () => {
  const {
    title,
    setTitle,
    reward,
    setReward,
    description,
    setDescription,

    createQuest,
    createIsLoading,
    createIsError,
    createIsSuccess,

    myQuests,
    myQuestsError,
    myQuestsLoading,
  } = useManageQuest();

  return (
    <PageLayout>
      <Box p={2}>
        <Container>
          <Typography variant="h4">Создание квеста</Typography>
          {createIsSuccess && (
            <Alert severity="success">Квест успешно создан</Alert>
          )}
          {createIsError && (
            <Alert severity="error">При создании квеста произошла ошибка</Alert>
          )}
          <TextField
            fullWidth
            label="Название"
            onChange={(e) => setTitle(e.target.value)}
            required
            size="small"
            sx={{ m: 1 }}
            value={title}
          />
          <TextField
            fullWidth
            label="Описание"
            minRows={2}
            multiline
            onChange={(e) => setDescription(e.target.value)}
            required
            size="small"
            sx={{ m: 1 }}
            value={description}
          />
          <TextField
            fullWidth
            label="Награда"
            multiline
            onChange={(e) => setReward(e.target.value)}
            required
            size="small"
            sx={{ m: 1 }}
            value={reward}
          />
          <LoadingButton
            loading={createIsLoading}
            variant="contained"
            onClick={() =>
              createQuest({ title, description, reward: Number(reward) })
            }
            sx={{ m: 1 }}
          >
            Создать
          </LoadingButton>
        </Container>
      </Box>
      <Box p={2}>
        <Container>
          <Typography variant="h4">Список созданных мной квестов:</Typography>
          {myQuestsLoading ? (
            <CircularProgress size={150} />
          ) : (
            myQuests?.map((quest: apiQuestData) => (
              <ListItemButton
                component="a"
                href={`/quest-asignees/${quest.id}`}
                key={quest.id}
              >
                <ListItemText primary={quest.title} />
              </ListItemButton>
            ))
          )}
        </Container>
      </Box>
    </PageLayout>
  );
};

export const ManageQuestPage = withAuth0UserProvider(ManageQuestComponent);
