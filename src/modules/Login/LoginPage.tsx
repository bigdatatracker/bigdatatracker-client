import React from 'react';
import { useAuth0 } from '@auth0/auth0-react';
import { Button } from '@mui/material';
import { useNavigate } from 'react-router-dom';

import { ROUTES_PATHS_MAP } from '../Core';
import { Loader } from '../../common/app-loader';
import { Background, PageTitle } from './components';
import About from './components/About';

export const LoginPage = () => {
  const { isAuthenticated, loginWithRedirect, isLoading } = useAuth0();
  const navigate = useNavigate();

  React.useEffect(() => {
    if (isAuthenticated) {
      navigate(ROUTES_PATHS_MAP.HOME);
    }
  }, [isAuthenticated, navigate]);

  if (isLoading) {
    return <Loader />;
  }

  return (
    <Background>
      <PageTitle variant="h1">BigData Tracker</PageTitle>
      <About>
        BigData Tracker является уникальным инструментом для менеджеров
        компании. Менеджеры компании при использовании приложения смогут оценить
        вовлеченность сотрудников в рабочую деятельность компании. Кроме того,
        приложение поможет сотрудникам компании чаще участвовать в рабочей жизни
        предприятия и налаживать корпоративные связи, таким образом повышая
        общую вовлеченность в деятельность компании всего коллектива.
      </About>
      <Button onClick={loginWithRedirect} variant="contained" size="large">
        Войти
      </Button>
    </Background>
  );
};
