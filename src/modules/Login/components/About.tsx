import React from 'react';
import { Typography, styled } from '@mui/material';

const StyledAbout = styled(Typography)({
  color: '#fff',
  maxWidth: '50%',
  textAlign: 'center',
});

const About = ({ children }: React.PropsWithChildren<unknown>) => {
  return <StyledAbout variant="subtitle1">{children}</StyledAbout>;
};

export default About;
