import React from 'react';
import { Typography, styled, TypographyProps } from '@mui/material';

const StyledTypography = styled(Typography)({
  color: '#fff',
  textTransform: 'uppercase',
  fontWeight: 'bold',
});

export const PageTitle = ({
  children,
  ...rest
}: React.PropsWithChildren<TypographyProps>) => {
  return <StyledTypography {...rest}>{children}</StyledTypography>;
};
