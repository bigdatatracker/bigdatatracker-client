import React from 'react';
import { styled } from '@mui/system';

import { AppPreview } from '../../../common/assets';

const BackgroundWrapper = styled('div')({
  position: `fixed`,
  inset: 0,
  backgroundImage: `url(${AppPreview})`,
  minHeight: '100vh',
  filter: 'brightness(50%)',
  width: '100%',
  backgroundRepeat: 'no-repeat',
  backgroundSize: 'cover',
});

const Content = styled('div')({
  position: `fixed`,
  minHeight: '100vh',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  flexDirection: 'column',
  gap: '40px',
  color: '#fff',
  width: '100%',
});

export const Background = ({ children }: React.PropsWithChildren<unknown>) => {
  return (
    <>
      <BackgroundWrapper />
      <Content>{children}</Content>
    </>
  );
};
