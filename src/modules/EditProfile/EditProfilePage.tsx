import React from 'react';

import { styled } from '@mui/material';

import { withAuth0UserProvider } from '../../common/HOC';
import { INDENT } from '../../common/tokens';

import { EditInfo } from './components/EditInfo/EditInfo';
import { useEditProfile } from './useEditProfile';
import { PageLayout } from '../PageLayout';
import { AUTH_HOC_PROPS } from '../../common/types';

const ContentWrapper = styled('div')({
  margin: `${INDENT.xxl} ${INDENT.xxl}`,
  display: 'flex',
  justifyContent: 'center',
});
const EditInfoWrapper = styled('div')({});

const EditProfileComponent = ({ userInfo }: AUTH_HOC_PROPS) => {
  const { data, loading: isProfileLoading } = useEditProfile(userInfo?.userId);

  return (
    <PageLayout>
      <ContentWrapper>
        <EditInfoWrapper>
          <EditInfo
            {...data}
            isProfileLoading={isProfileLoading}
            isEmpty={!data}
          />
        </EditInfoWrapper>
      </ContentWrapper>
    </PageLayout>
  );
};

export const EditProfilePage = withAuth0UserProvider(EditProfileComponent);
