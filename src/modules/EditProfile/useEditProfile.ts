import { useQuery } from 'react-query';

import { ajaxEvents } from '../../common/http';

export const useEditProfile = (id: string | undefined) => {
  const getUserById = async () => {
    const { data } = await ajaxEvents.request<any>({
      method: 'GET',
      url: `/users/${id}`,
    });

    return data;
  };

  const {
    data,
    isError: error,
    isLoading,
    isFetching,
  } = useQuery(['usersProfile', id], () => getUserById(), {
    refetchOnWindowFocus: false,
    enabled: !!id,
  });

  return {
    data,
    error,
    loading: isFetching || isLoading,
  };
};
