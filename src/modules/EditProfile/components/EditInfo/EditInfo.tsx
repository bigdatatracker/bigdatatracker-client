import React from 'react';
import { useMutation } from 'react-query';

import {
  styled,
  Typography,
  Avatar,
  TextField,
  CircularProgress,
} from '@mui/material';
import StarIcon from '@mui/icons-material/Star';
import SaveIcon from '@mui/icons-material/Save';
import LoadingButton from '@mui/lab/LoadingButton';

import { EditInfoProps } from './types';
import { ajaxEvents } from '../../../../common/http';
import { INDENT } from '../../../../common/tokens';

const EditInfoWrapper = styled('div')({
  width: '90vw',
});

const AvatarWrapper = styled('div')({
  display: 'flex',
  alignContent: 'center',
  alignItems: 'center',
});

const NameWrapper = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  alignContent: 'center',
  alignItems: 'center',
});

const BalanceWrapper = styled('div')({
  display: 'flex',
  alignItems: 'center',
});

const EditFieldWrapper = styled('div')({
  marginTop: 10,
});

const updateUser = async ({
  id,
  bio,
  position,
}: {
  id: string;
  bio: string;
  position: string;
}) => {
  return ajaxEvents.request<any>({
    method: 'PATCH',
    url: `/users/${id}`,
    data: {
      bio,
      position,
    },
  });
};

export const EditInfo = ({
  id,
  name,
  picture,
  bio = '',
  balance,
  position = '',
  isProfileLoading,
  isEmpty,
}: EditInfoProps) => {
  const [userPosition, setUserPosition] = React.useState('');
  const [userBio, setUserBio] = React.useState('');

  const { mutate, isLoading: isUpdateUserLoading } = useMutation(updateUser);

  React.useEffect(() => {
    setUserPosition(position);
    setUserBio(bio);
  }, [position, bio]);

  if (isProfileLoading) {
    return <CircularProgress />;
  }

  if (isEmpty) {
    return null;
  }

  return (
    <EditInfoWrapper>
      <AvatarWrapper>
        <Avatar
          src={picture}
          alt={name}
          sx={{ width: 80, height: 80, marginRight: 5 }}
        />
        <NameWrapper>
          <Typography variant="h6">{name} </Typography>
          <BalanceWrapper>
            <StarIcon color="success" />
            <Typography fontSize={16}>{balance}</Typography>
          </BalanceWrapper>
        </NameWrapper>
      </AvatarWrapper>
      <EditFieldWrapper>
        <Typography fontSize={14} fontWeight={600} color="primary">
          Должность
        </Typography>
        <TextField
          size="small"
          value={userPosition}
          onChange={(e) => setUserPosition(e.target.value)}
          fullWidth
        />
      </EditFieldWrapper>
      <EditFieldWrapper>
        <Typography fontSize={14} fontWeight={600} color="primary">
          О себе
        </Typography>
        <TextField
          size="small"
          multiline
          value={userBio}
          onChange={(e) => setUserBio(e.target.value)}
          fullWidth
        />
      </EditFieldWrapper>
      <LoadingButton
        loading={isUpdateUserLoading}
        loadingPosition="start"
        variant="outlined"
        startIcon={<SaveIcon />}
        onClick={() => mutate({ id, bio: userBio, position: userPosition })}
        sx={{ marginTop: `${INDENT.xs}` }}
      >
        Сохранить
      </LoadingButton>
    </EditInfoWrapper>
  );
};
