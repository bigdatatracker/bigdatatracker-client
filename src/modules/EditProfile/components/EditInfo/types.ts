export type EditInfoProps = {
  id: string;
  name: string;
  role: string;
  bio: string;
  status: string;
  picture: string;
  balance: string;
  position: string;
  isProfileLoading?: boolean;
  isEmpty?: boolean;
};
