import React from 'react';
import { styled } from '@mui/material';

import { usePosts } from './hooks';
import { NewPostField, Posts } from './components';
import { PageLayout } from '../PageLayout';
import { INDENT } from '../../common/tokens';
import { withAuth0UserProvider } from '../../common/HOC';
import { AuthUserContext } from '../../common/context';

const PostsWrapper = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  maxWidth: 500,
  margin: `${INDENT.none} ${INDENT.auto}`,
});

const HomePageComponent = () => {
  const { userInfo } = React.useContext(AuthUserContext);
  const { createNewPost, isPostsLoading, userPosts } = usePosts(
    userInfo?.userId,
  );

  return (
    <PageLayout>
      <PostsWrapper>
        <NewPostField onPostAdd={createNewPost} />
        <Posts userPosts={userPosts} isLoading={isPostsLoading} />
      </PostsWrapper>

      {/* <UserInfo userInfo={userInfo} isLoading={userDataLoading} /> */}
    </PageLayout>
  );
};

export const HomePage = withAuth0UserProvider(HomePageComponent);
