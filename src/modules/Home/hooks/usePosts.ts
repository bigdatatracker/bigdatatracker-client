import { useMutation, useQuery } from 'react-query';

import { PostType } from '../../../common/types';
import { getPosts, createPost } from '../requests';

export const usePosts = (id?: string) => {
  const {
    data: userPosts,
    refetch,
    isLoading,
    isFetching,
    isRefetching,
  } = useQuery<PostType[]>(['user_posts', id], () => getPosts(id), {
    refetchOnWindowFocus: false,
  });

  const handleCreatePost = async (content: string) => {
    await createPost(content);
    await refetch();
  };

  const { mutate: createNewPost, isLoading: isMutating } =
    useMutation(handleCreatePost);

  return {
    isPostsLoading: isLoading || isFetching || isRefetching || isMutating,
    createNewPost,
    userPosts,
  };
};
