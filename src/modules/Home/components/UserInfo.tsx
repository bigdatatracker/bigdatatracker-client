import React from 'react';
import { CircularProgress, styled } from '@mui/material';

import { AUTH0_USER } from '../../../common/types';

const UserInfoWrapper = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  minHeight: '500px',
  justifyContent: 'center',
  alignItems: 'center',
  gap: '30px',
});

type UserInfoProps = {
  userInfo?: AUTH0_USER;
  isLoading: boolean;
};

// TODO - убрать потом в Profile user-a
export const UserInfo = ({ userInfo, isLoading }: UserInfoProps) => {
  const userInfoContent = isLoading ? (
    <CircularProgress />
  ) : (
    <>
      <h2>User info</h2>
      <div>Email: {userInfo?.user?.email}</div>
      <div>Nickname: {userInfo?.user?.nickname}</div>
      <div>
        <img src={userInfo?.user?.picture} alt="User" width={50} height={50} />
      </div>
      <div>Location: {userInfo?.user_metadata?.location}</div>
      <div>First name: {userInfo?.user_metadata?.first_name}</div>
    </>
  );

  return <UserInfoWrapper>{userInfoContent}</UserInfoWrapper>;
};
