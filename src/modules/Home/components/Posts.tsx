import React from 'react';
import CircularProgress from '@mui/material/CircularProgress';

import { styled } from '@mui/system';
import { PostType } from '../../../common/types';

import { Post } from './Post';
import { INDENT } from '../../../common/tokens';

type PostsProps = {
  userPosts?: PostType[];
  isLoading?: boolean;
};

const PostsLoader = styled(CircularProgress)({
  margin: `${INDENT.none} ${INDENT.auto}`,
});

export const Posts = ({ userPosts, isLoading }: PostsProps) => {
  if (isLoading) {
    return <PostsLoader />;
  }

  if (!userPosts) {
    return null;
  }

  return (
    <>
      {userPosts.map((post, index) => (
        <Post
          // eslint-disable-next-line react/no-array-index-key
          key={`${index}_${post.content}`}
          title={post.content}
          postIndex={index + 1}
        />
      ))}
    </>
  );
};
