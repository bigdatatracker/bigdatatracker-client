import React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import { COLORS, INDENT } from '../../../common/tokens';

type PostProps = {
  title: string;
  postIndex: number;
};

export const Post = ({ title, postIndex }: PostProps) => {
  return (
    <Card sx={{ margin: `${INDENT.s}` }} raised>
      <CardContent>
        <Typography gutterBottom variant="h5" component="div">
          Post #{postIndex}
        </Typography>
        <Typography variant="body2" color={COLORS.black}>
          {title}
        </Typography>
      </CardContent>
    </Card>
  );
};
