export const mockPosts = [
  {
    title: 'Пост 1',
    postIndex: 1,
  },
  {
    title: 'Пост 2',
    postIndex: 2,
  },
  {
    title: 'Пост 3',
    postIndex: 3,
  },
  {
    title: 'Пост 4',
    postIndex: 4,
  },
  {
    title: 'Пост 5',
    postIndex: 5,
  },
];
