import React from 'react';
import { Fab, styled, TextField } from '@mui/material';

import { INDENT } from '../../../common/tokens';

const PostInputWrapper = styled('div')({
  display: 'flex',
  alignItems: 'center',
  gap: `${INDENT.xs}`,
});

const PostInput = styled(TextField)({
  width: '100%',
  margin: `${INDENT.xxs} ${INDENT.none}`,
});

const ButtonIcon = styled(Fab)({
  flexShrink: 0,
});

type NewPostFieldProps = {
  onPostAdd: (title: string) => void;
};

export const NewPostField = ({ onPostAdd }: NewPostFieldProps) => {
  const [inputValue, setInputValue] = React.useState<string>('');

  const handlePostAdd = () => {
    if (inputValue) {
      onPostAdd(inputValue);
      setInputValue('');
    }
  };

  return (
    <PostInputWrapper>
      <PostInput
        placeholder="Что у вас нового?"
        onChange={(event) => setInputValue(event.target.value)}
        value={inputValue}
      />
      <ButtonIcon
        color="primary"
        aria-label="add"
        size="medium"
        onClick={handlePostAdd}
      >
        +
      </ButtonIcon>
    </PostInputWrapper>
  );
};
