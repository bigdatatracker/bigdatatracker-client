import { PostType } from '../../../common/types';

export const ADD_POST = 'ADD_POST';
export const INIT = 'INIT';

export const initializePosts = (posts: PostType[]) => {
  return {
    type: INIT,
    payload: posts,
  };
};

export const addStorePost = (newPost: PostType) => {
  return {
    type: ADD_POST,
    payload: newPost,
  };
};
