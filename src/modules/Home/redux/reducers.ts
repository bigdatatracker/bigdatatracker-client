import { Reducer } from 'redux';

import { ADD_POST, INIT } from './actions';
import { Action, State } from './types';

const INITIAL_STATE: State = {
  postsList: [],
};

export const postReducer: Reducer<State, Action> = (
  state = INITIAL_STATE,
  action,
) => {
  const prevPosts = state?.postsList ?? [];

  switch (action.type) {
    case INIT:
      return { postsList: action.payload };

    case ADD_POST:
      return { postsList: [...prevPosts, action.payload] };

    default:
      return state;
  }
};
