import { PostType } from '../../../common/types';

export type State = {
  postsList: PostType[];
};

export type SelectorType = {
  posts: State;
};

export type Action =
  | { type: 'ADD_POST'; payload: PostType }
  | { type: 'INIT'; payload: PostType[] };
