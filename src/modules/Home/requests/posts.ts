import { ajaxEvents } from '../../../common/http';
import { PostType } from '../../../common/types';

export const getPosts = async (userId?: string) => {
  if (!userId) {
    return [];
  }

  const {
    data: { items },
  } = await ajaxEvents.request<{ items: PostType[] }>({
    method: 'GET',
    url: `/users/${userId}/posts`,
    params: {
      limit: 50,
    },
  });

  return items.reverse().slice(0, 5);
};

export const createPost = async (content: string) => {
  await ajaxEvents.request({
    method: 'POST',
    url: '/posts',
    data: {
      content,
    },
  });
};
