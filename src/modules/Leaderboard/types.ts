export type UserInfoProps = {
  name: string;
  role: string;
  status: string;
  picture: string;
  balance: number;
  position: string;
  id: string;
  email: string;
};
