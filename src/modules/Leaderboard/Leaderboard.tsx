import { Error } from '@mui/icons-material';
import {
  Avatar,
  Box,
  CircularProgress,
  Container,
  Paper,
  styled,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
  Checkbox,
  FormControlLabel,
} from '@mui/material';
import { tableCellClasses } from '@mui/material/TableCell';
import React from 'react';

import { withAuth0UserProvider } from '../../common/HOC';
import { PageLayout } from '../PageLayout';
import { useLeaderboard } from './useLeaderboard';
import { UserInfoProps } from './types';

const User = styled('div')(() => ({
  display: 'flex',
  justifyItems: 'center',
  alignItems: 'center',
  flexWrap: 'wrap',
  marginLeft: 5,
}));

const StyledTableCell = styled(TableCell)(() => ({
  [`&.${tableCellClasses.head}`]: {
    fontWeight: 'bold',
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const LeaderboardPage = () => {
  const { leaderboard, loading, error, isManager, handleCheckBoxChange } =
    useLeaderboard();

  if (loading) {
    return <CircularProgress />;
  }

  if (error) {
    return <Error />;
  }

  return (
    <PageLayout>
      <Box p={2}>
        <Container>
          <Typography variant="h4" sx={{ mt: 3, mb: 3 }}>
            Рейтинг пользователей
          </Typography>
          {isManager && (
            <FormControlLabel
              label="Показать только своих подчиненных"
              control={
                <Checkbox
                  defaultChecked={false}
                  onChange={handleCheckBoxChange}
                />
              }
            />
          )}

          <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="leaderboard">
              <TableHead>
                <TableRow>
                  <StyledTableCell>Имя</StyledTableCell>
                  <StyledTableCell align="right">Должность</StyledTableCell>
                  <StyledTableCell align="right">Рейтинг</StyledTableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {leaderboard &&
                  leaderboard.length > 0 &&
                  leaderboard
                    .sort(
                      (user1: UserInfoProps, user2: UserInfoProps) =>
                        user2.balance - user1.balance,
                    )
                    .map((user: UserInfoProps) => (
                      <TableRow
                        key={user.email}
                        sx={{
                          '&:last-child td, &:last-child th': { border: 0 },
                        }}
                      >
                        <StyledTableCell component="th" scope="row">
                          <User>
                            <Avatar alt={user.name} src={user.picture} />
                            <Typography paragraph={false} marginLeft={1}>
                              {user.name}
                            </Typography>
                          </User>
                        </StyledTableCell>
                        <StyledTableCell align="right">
                          {user.position}
                        </StyledTableCell>
                        <StyledTableCell align="right">
                          {user.balance}
                        </StyledTableCell>
                      </TableRow>
                    ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Container>
      </Box>
    </PageLayout>
  );
};

export const Leaderboard = withAuth0UserProvider(LeaderboardPage);
