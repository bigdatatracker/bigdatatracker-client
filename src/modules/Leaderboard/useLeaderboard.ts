import React from 'react';
import { useQuery } from 'react-query';

import { ajaxEvents } from '../../common/http';
import { AuthUserContext } from '../../common/context';

export const useLeaderboard = () => {
  const { userInfo } = React.useContext(AuthUserContext);
  const isManager = userInfo?.isManager;
  const userId = userInfo?.userId;
  const [currentManager, setCurrentManager] = React.useState(false);

  const getLeaderboard = async () => {
    const { data } = await ajaxEvents.request<any>({
      method: 'GET',
      url: '/users',
      params: currentManager ? { managerId: userId } : null,
    });

    return data.items;
  };

  const {
    data: leaderboard = [],
    isError: error,
    isLoading: loading,
  } = useQuery(['leaderboard', currentManager], () => getLeaderboard(), {
    refetchOnWindowFocus: false,
  });

  const handleCheckBoxChange = () => {
    setCurrentManager(!currentManager);
  };

  return {
    error,
    loading,
    leaderboard,
    isManager,
    handleCheckBoxChange,
  };
};
