import React, { useMemo } from 'react';
import { Link, matchPath, useLocation, useNavigate } from 'react-router-dom';
import { styled, alpha } from '@mui/material/styles';
import {
  AppBar,
  Box,
  Toolbar,
  IconButton,
  Typography,
  Menu,
  Container,
  Avatar,
  Button,
  Tooltip,
  MenuItem,
  InputBase,
} from '@mui/material';
import { Menu as MenuIcon, Search as SearchIcon } from '@mui/icons-material';

import { UsersSearch } from '../UsersSearch';
import { ROUTES_PATHS_MAP } from '../Core';

import { useHeader } from './useHeader';
import { AuthUserContext } from '../../common/context';

const pages = [
  {
    name: 'Главная',
    link: ROUTES_PATHS_MAP.HOME,
  },
  {
    name: 'Квесты',
    link: ROUTES_PATHS_MAP.QUESTS,
  },
  {
    name: 'Список лидеров',
    link: ROUTES_PATHS_MAP.LEADERBOARD,
  },
];

const Search = styled('div')(({ theme }) => ({
  position: 'relative',
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  '&:hover': {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
  marginLeft: 0,
  width: '100%',
  [theme.breakpoints.up('sm')]: {
    marginLeft: theme.spacing(1),
    width: 'auto',
  },
}));

const MenuLink = styled(Link)`
  text-decoration: none;
  color: #000;
`;

const SearchIconWrapper = styled('div')(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: '100%',
  position: 'absolute',
  pointerEvents: 'none',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: 'inherit',
  '& .MuiInputBase-input': {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: '12ch',
      '&:focus': {
        width: '20ch',
      },
    },
  },
}));

export const Header = () => {
  const { userInfo, logout } = React.useContext(AuthUserContext);
  const [anchorEl, setAnchorEl] = React.useState<HTMLElement | null>(null);
  const navigate = useNavigate();
  const { pathname } = useLocation();
  const open = Boolean(anchorEl);

  const avatar = useMemo(
    () => userInfo?.user?.picture,
    [userInfo?.user?.picture],
  );

  const logoutWithRedirect = () =>
    logout({
      returnTo: window.location.origin,
    });

  const {
    localInputValue,
    setLocalInputValue,
    debounceValue,
    usersList,
    error,
    loading,
  } = useHeader();

  const isManagerValue = userInfo?.isManager;
  const isManager = isManagerValue === undefined ? false : isManagerValue;
  const settings = [
    { name: 'Профиль', link: `/profiles/${userInfo?.userId}` },
    { name: 'Изменить профиль', link: ROUTES_PATHS_MAP.EDIT_PROFILE },
  ];

  return (
    <>
      <AppBar position="static">
        <Container maxWidth="xl">
          <Toolbar disableGutters>
            <Typography
              variant="h6"
              noWrap
              component="div"
              sx={{
                mr: 2,
                display: { xs: 'none', md: 'flex' },
                cursor: 'pointer',
              }}
              onClick={() => navigate(ROUTES_PATHS_MAP.HOME)}
            >
              BIGDATA
            </Typography>
            <Search>
              <SearchIconWrapper>
                <SearchIcon />
              </SearchIconWrapper>
              <StyledInputBase
                placeholder="Поиск…"
                inputProps={{ 'aria-label': 'search' }}
                value={localInputValue}
                onChange={(event) => {
                  setLocalInputValue(event.target.value);
                }}
              />
            </Search>
            <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
              <IconButton
                size="large"
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                color="inherit"
              >
                <MenuIcon />
              </IconButton>
              <Menu
                id="menu-appbar"
                anchorOrigin={{
                  vertical: 'bottom',
                  horizontal: 'left',
                }}
                keepMounted
                transformOrigin={{
                  vertical: 'top',
                  horizontal: 'left',
                }}
                open={false}
                sx={{
                  display: { xs: 'block', md: 'none' },
                }}
              >
                {pages.map(({ name, link }) => (
                  <MenuItem key={name} LinkComponent={Link} href={link}>
                    <Typography textAlign="center">{name}</Typography>
                  </MenuItem>
                ))}
              </Menu>
            </Box>
            <Typography
              variant="h6"
              noWrap
              component="div"
              sx={{
                flexGrow: 1,
                display: { xs: 'none', sm: 'flex', md: 'none' },
              }}
            >
              BIGDATA
            </Typography>
            <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
              {pages.map(({ name, link }) => (
                <Button
                  key={name}
                  component={Link}
                  to={link}
                  sx={{
                    my: 2,
                    color: matchPath(`${link}`, pathname)
                      ? 'white'
                      : 'lightblue',
                  }}
                >
                  {name}
                </Button>
              ))}
              {isManager && (
                <Button
                  component={Link}
                  to="/quests/manage"
                  sx={{
                    my: 2,
                    color: matchPath('/quests/manage', pathname)
                      ? 'white'
                      : 'lightblue',
                  }}
                >
                  Управление квестами
                </Button>
              )}
            </Box>
            <Box sx={{ flexGrow: 0 }}>
              <Tooltip title="Меню">
                <IconButton
                  sx={{ p: 0 }}
                  onClick={(e) => setAnchorEl(e.currentTarget)}
                >
                  <Avatar alt="Remy Sharp" src={avatar} />
                </IconButton>
              </Tooltip>
              <Menu
                sx={{ mt: '45px' }}
                id="menu-appbar"
                anchorOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                keepMounted
                transformOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                anchorEl={anchorEl}
                onClose={() => setAnchorEl(null)}
                open={open}
              >
                {settings.map((setting, index) => (
                  // eslint-disable-next-line react/no-array-index-key
                  <MenuLink to={setting.link} key={`${setting.link}_${index}`}>
                    <MenuItem>
                      <Typography textAlign="center">{setting.name}</Typography>
                    </MenuItem>
                  </MenuLink>
                ))}
                <MenuItem onClick={logoutWithRedirect}>
                  <Typography textAlign="center">Выйти</Typography>
                </MenuItem>
              </Menu>
            </Box>
          </Toolbar>
        </Container>
      </AppBar>
      <UsersSearch
        searchFieldText={debounceValue}
        loading={loading}
        usersList={usersList}
        error={error}
      />
    </>
  );
};
