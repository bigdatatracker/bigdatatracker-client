import React from 'react';
import { useQuery } from 'react-query';
import { useLocation } from 'react-router-dom';

import { ajaxEvents } from '../../common/http';
import { useDebounce } from '../../common/hooks';

export const useHeader = () => {
  const location = useLocation();
  const [localInputValue, setLocalInputValue] = React.useState<string>('');
  const debounceValue = useDebounce(localInputValue, 300);

  React.useEffect(() => {
    setLocalInputValue('');
  }, [location]);

  const getUsersList = async (searchFieldValue: string) => {
    const { data } = await ajaxEvents.request<any>({
      method: 'GET',
      url: '/users/',
      params: { q: searchFieldValue },
    });

    return data;
  };

  const {
    data,
    isError: error,
    isLoading: loading,
  } = useQuery(
    ['usersList', debounceValue],
    () => getUsersList(debounceValue),
    {
      enabled: !!debounceValue,
    },
  );

  const usersList = data?.items;

  return {
    localInputValue,
    setLocalInputValue,
    usersList,
    error,
    loading,
    debounceValue,
  };
};
