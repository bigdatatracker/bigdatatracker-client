import { DB_USER } from '../../common/types';

export type UsersSearchProps = {
  usersList: DB_USER[];
  searchFieldText: string;
  error: boolean;
  loading: boolean;
};
