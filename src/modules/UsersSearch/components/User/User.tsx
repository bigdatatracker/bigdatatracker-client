import React from 'react';
import {
  ListItem,
  Divider,
  ListItemText,
  ListItemAvatar,
  Avatar,
} from '@mui/material';
import { Link } from 'react-router-dom';

import { DB_USER } from '../../../../common/types';

export const User = ({ id, name, picture }: DB_USER) => (
  <>
    <ListItem
      component={Link}
      to={`/profiles/${id}`}
      alignItems="flex-start"
      sx={{ cursor: 'pointer' }}
    >
      <ListItemAvatar>
        <Avatar alt={name} src={picture} />
      </ListItemAvatar>
      <ListItemText
        primaryTypographyProps={{ style: { color: 'black' } }}
        primary={name}
        secondary={<>Статус</>}
      />
    </ListItem>
    <Divider variant="inset" component="li" />
  </>
);
