import React from 'react';
import { CircularProgress, List, Typography, styled } from '@mui/material';

import { User } from './components';
import { UsersSearchProps } from './types';

const ContentContainer = styled('div')({
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
});

export const UsersSearch = ({
  searchFieldText,
  loading,
  usersList,
}: UsersSearchProps) => (
  <div>
    {searchFieldText && (
      <List
        sx={{
          position: 'absolute',
          width: '100%',
          maxWidth: 360,
          minWidth: 100,
          bgcolor: 'background.paper',
          zIndex: 1,
        }}
      >
        {loading ? (
          <ContentContainer>
            <CircularProgress />
          </ContentContainer>
        ) : (
          <div>
            {usersList.length ? (
              usersList.map((user) => <User key={user.id} {...user} />)
            ) : (
              <ContentContainer>
                <Typography variant="h6" noWrap component="div">
                  Нет результатов
                </Typography>
              </ContentContainer>
            )}
          </div>
        )}
      </List>
    )}
  </div>
);
