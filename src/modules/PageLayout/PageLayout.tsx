import React from 'react';

import { Header } from '../Header';

export const PageLayout = ({ children }: React.PropsWithChildren<unknown>) => {
  return (
    <>
      <Header />
      {children}
    </>
  );
};
