import { combineReducers } from 'redux';

import { postReducer } from '../modules/Home/redux/reducers';

export const rootReducer = combineReducers({
  posts: postReducer,
});
