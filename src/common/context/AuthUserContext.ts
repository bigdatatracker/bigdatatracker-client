import React from 'react';
import { AUTH_HOC_PROPS } from '../types';

export const AuthUserContext = React.createContext<AUTH_HOC_PROPS>({
  userInfo: undefined,
  logout: () => {},
  userDataLoading: false,
});
