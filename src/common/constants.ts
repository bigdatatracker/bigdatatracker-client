import { createTheme } from '@mui/material/styles';

// каст из-за того, что процесс дает string | undefined
export const APP_DOMAIN = process.env.REACT_APP_AUTH0_DOMAIN as string;
export const CLIENT_ID = process.env.REACT_APP_AUTH0_CLIEND_ID as string;
export const AUTH0_AUDIENCE = process.env.REACT_APP_AUTH0_AUDIENCE as string;
export const API_URL = process.env.REACT_APP_AXIOS_BASE_URL as string;
export const AUTH0_SCOPE =
  'read:current_user update:current_user_metadata read:roles';

export const PRODUCTION_NAMESPACE = process.env
  .REACT_APP_PRODUCTION_NAMESPACE as string;

export const THEME = createTheme({
  typography: {
    fontFamily: `"Archivo", sans-serif`,
    fontSize: 14,
    fontWeightLight: 300,
    fontWeightRegular: 400,
    fontWeightMedium: 500,
  },
});
