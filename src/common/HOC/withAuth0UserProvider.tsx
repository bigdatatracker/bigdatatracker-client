import { withAuthenticationRequired } from '@auth0/auth0-react';
import React, { ComponentType, FC } from 'react';

import { Loader } from '../app-loader';
import { AuthUserContext } from '../context';

import { useUserAuth, useUpdateUserInfo } from '../hooks';

export const withAuth0UserProvider = <P extends object>(
  Component: ComponentType<P>,
): FC<P> => {
  const WithAuth0User = (props: P): JSX.Element => {
    const { userInfo, logout, userDataLoading } = useUserAuth();
    useUpdateUserInfo(userInfo?.user);

    const memoValue = React.useMemo(
      () => ({
        userInfo,
        logout,
        userDataLoading,
      }),
      [userInfo, logout, userDataLoading],
    );

    return (
      <AuthUserContext.Provider value={memoValue}>
        <Component
          userInfo={userInfo}
          logout={logout}
          userDataLoading={userDataLoading}
          {...props}
        />
      </AuthUserContext.Provider>
    );
  };

  return withAuthenticationRequired(WithAuth0User, {
    onRedirecting: () => <Loader />,
  });
};
