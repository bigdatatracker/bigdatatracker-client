import React from 'react';

type RouteType = {
  path: string;
  /**
   * any for class component types
   * link https://github.com/DefinitelyTyped/DefinitelyTyped/blob/674a7033d617dff421b631e7acf7fd4f04bc8a5c/types/react/index.d.ts#:~:text=type%20LazyExoticComponent
   */
  component: React.LazyExoticComponent<React.ComponentType<any>>;
  pageName: string;
};

export type RouterConfig = RouteType[];
