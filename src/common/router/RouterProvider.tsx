import React from 'react';
import { BrowserRouter as Router, Routes } from 'react-router-dom';

export const RouterProvider = ({
  children,
}: React.PropsWithChildren<unknown>) => {
  return (
    <Router>
      <Routes>{children}</Routes>
    </Router>
  );
};
