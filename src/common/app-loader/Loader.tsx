import React from 'react';
import Lottie from 'react-lottie';
import { CircularProgress, styled } from '@mui/material';

import { useBreakpointState } from '../breakpoints';
import groovyWalkAnimation from './groovyWalk.json';

const options = {
  animationData: groovyWalkAnimation,
  loop: true,
  autoplay: true,
};

const LoaderContainer = styled('div')({
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  height: '100vh',
});

export const Loader = () => {
  const { isMobile } = useBreakpointState();

  if (isMobile) {
    return (
      <LoaderContainer>
        <CircularProgress size={150} />
      </LoaderContainer>
    );
  }

  return <Lottie options={options} height="100vh" />;
};
