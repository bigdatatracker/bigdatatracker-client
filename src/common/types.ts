import { LogoutOptions, User } from '@auth0/auth0-react';

export type AUTH0_USER = {
  user: User | undefined;
  userId?: string;
  isManager: boolean;
  user_metadata: {
    first_name: string;
    location: string;
  };
};

// Объект пользователя, который возвращает бэк
export type DB_USER = Pick<User, 'email' | 'name' | 'picture'> & {
  id: string;
  role: UserRole;
};

export type UserRole = 'employee' | 'manager';

export type PostType = {
  attachment?: string;
  content: string;
  creator: DB_USER;
};

export type AUTH_HOC_PROPS = {
  userInfo: AUTH0_USER | undefined;
  logout: (options?: LogoutOptions | undefined) => void;
  userDataLoading: boolean;
};
