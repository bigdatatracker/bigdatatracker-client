import { User } from '@auth0/auth0-react';

import { PRODUCTION_NAMESPACE } from '../constants';
import { UserRole } from '../types';

export const getUserDataWithNamespace = ({
  value,
  user,
}: {
  value: string;
  user?: User;
}): UserRole => {
  if (!user) {
    return 'employee';
  }

  const key = `${PRODUCTION_NAMESPACE}${value}`;
  return user[key][0];
};

export const getCurrentUserId = (user?: User) => {
  if (!user) {
    return undefined;
  }

  const { sub } = user;

  const id = sub?.split('|')[1];

  return id;
};
