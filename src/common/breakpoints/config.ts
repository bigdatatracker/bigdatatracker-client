export const BREAKPOINT_NAMES = {
  mobile: 'mobile',
  tablet: 'tablet',
  desktopS: 'desktopS',
  desktopM: 'desktopM',
  desktopL: 'desktopL',
} as const;

export type BREAKPOINT_NAMES_KEYS = keyof typeof BREAKPOINT_NAMES;

export const breakpoints: Record<BREAKPOINT_NAMES_KEYS, number> = {
  mobile: 320,
  tablet: 768,
  desktopS: 1024,
  desktopM: 1200,
  desktopL: 1400,
};
