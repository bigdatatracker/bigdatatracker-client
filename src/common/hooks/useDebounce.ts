import React from 'react';

export const useDebounce = <T>(value: T, delay: number): T => {
  const [debounceValue, setDebounceValue] = React.useState<T>(value);
  const timeout = React.useRef(-1);

  React.useEffect(() => {
    const handler = () => {
      clearTimeout(timeout.current);

      timeout.current = window.setTimeout(() => {
        setDebounceValue(value);
      }, delay);
    };

    handler();
  }, [delay, value]);

  return debounceValue;
};
