import React from 'react';
import axios from 'axios';
import { useAuth0 } from '@auth0/auth0-react';

import { APP_DOMAIN } from '../constants';
import { AUTH0_USER } from '../types';
import { getCurrentUserId } from '../utils';
import { getUserDataWithNamespace } from '../utils';

export const useUserAuth = () => {
  const {
    logout,
    getAccessTokenSilently,
    user,
    isLoading: isAuthLoaing,
  } = useAuth0();

  const [userInfo, setUserInfo] = React.useState<AUTH0_USER>();
  const [userDataLoading, setUserDataLoading] = React.useState(false);

  React.useEffect(() => {
    const getUserMetadata = async () => {
      try {
        setUserDataLoading(true);
        const accessToken = await getAccessTokenSilently({
          audience: `https://${APP_DOMAIN}/api/v2/`,
          scope: 'read:current_user read:roles',
        });

        // TODO - сделать утилку
        localStorage.setItem('accessToken', accessToken);

        const userDetailsByIdUrl = `https://${APP_DOMAIN}/api/v2/users/${user?.sub}`;

        const { data } = await axios.get(userDetailsByIdUrl, {
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
        });

        const { user_metadata } = data;

        const isAllManagersFeature =
          Boolean(localStorage.getItem('isAllManagers')) || false;

        const isManager =
          getUserDataWithNamespace({
            value: 'roles',
            user,
          }) === 'manager' || isAllManagersFeature;

        const userInfoData = {
          userId: getCurrentUserId(user),
          isManager,
          user,
          user_metadata,
        };

        setUserInfo(userInfoData);
        setUserDataLoading(false);
      } catch {
        console.log('Error with fetching user');
      }
    };

    getUserMetadata();
  }, [getAccessTokenSilently, setUserInfo, user]);

  return {
    userInfo,
    isAuthLoaing,
    logout,
    userDataLoading,
  };
};
