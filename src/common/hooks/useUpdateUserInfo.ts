import React from 'react';
import { User } from '@auth0/auth0-react';

import { ajaxEvents } from '../http';

const UPDATE_USERS_ENDPOINT = '/users/update-current';

export const useUpdateUserInfo = (user: User | undefined) => {
  React.useEffect(() => {
    const isUpdateInCurrentSession = sessionStorage.getItem('isUserUpdated');

    if (user && !isUpdateInCurrentSession) {
      ajaxEvents.request({
        url: UPDATE_USERS_ENDPOINT,
        method: 'POST',
      });

      sessionStorage.setItem('isUserUpdated', 'DONE');
    }
  }, [user]);
};
