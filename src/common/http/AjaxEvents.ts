/* eslint-disable no-param-reassign */
import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';

import { API_URL } from '../constants';

class AjaxEvents {
  private readonly axiosInstance: AxiosInstance;

  constructor() {
    this.axiosInstance = axios.create({
      baseURL: API_URL,
    });

    this.axiosInstance.interceptors.request.use((config) => {
      if (config.headers) {
        config.headers.Authorization = `Bearer ${localStorage.getItem(
          'accessToken',
        )}`;
      }

      return config;
    });
  }

  request<T>(config: AxiosRequestConfig): Promise<AxiosResponse<T>> {
    return this.axiosInstance.request(config);
  }
}

export const ajaxEvents = new AjaxEvents();
